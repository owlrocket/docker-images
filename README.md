# Owl Rocket Docker Images
A set of docker images that are precompiled to save time. GRPC in particular, takes a very long time to compile.

## Images
- `or-php-grpc` PHP:apache (latest)
  - g++, zlib1g-dev, git, zip, libyaml-dev
  - GRPC
  - Yaml
  - Composer